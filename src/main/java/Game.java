
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author napas
 */
public class Game {
    private Player playerX;
    private Player playerO;
    private Player turn;
    private Table table;
    private int row,col;
    private char restart;
    Scanner kb = new Scanner(System.in);
    public Game(){
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX,playerO);
    }
    public void input(){
         while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if(table.setRowCol(row, col)){
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }
    public void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    public void showTable(){
        table.showTable();
    }
    public void showTurn(){
        System.out.println(table.getCurrentPlayer().getName()+" turn");
    }
    public void showBye(){
        System.out.println("Bye bye...");
    }
    public void newGame(){
        while(true){
        System.out.println("Restart Game? (Y/N): ");
        restart = kb.next().charAt(0);
        if(restart=='Y'){
            table = new Table(playerX,playerO);
            break;
        }else if(restart=='N'){
            System.out.println("Thank you for playing!!!");
            break;
        }else{
            System.out.println("Error!! Please try again.");
        }
        
        }
    }
    public void run(){
        this.showWelcome();
        while(true){
        this.showTable();
        this.showTurn();
        this.input();
        table.checkWin();
        if(table.isFinish()){
            if(table.getWinner()==null){
                System.out.println("Draw!!");
            }else{
                System.out.println(table.getWinner().getName()+" Win!!");
            }
            this.showTable();
            newGame();
            if(restart=='N'){
                break;
            }
        }
        table.switchPlayer();
        }
        this.showBye();
        
        
    }
    
}
